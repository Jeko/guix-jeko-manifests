(specifications->manifest
 '(
   ;; Writing
   "skribilo"

   ;; Bash
   "shellcheck"
   "direnv"
   "tree"

   ;; Emacs exports
   "texlive"
   
   ;; Emacs
   "emacs"
   "emacs-which-key"
   "emacs-all-the-icons"
   "emacs-doom-modeline"
   "emacs-minions"
   "emacs-markdown-mode"
   "emacs-flycheck-grammalecte"
   "emacs-flymake-shellcheck"
   "emacs-paredit"
   "emacs-yasnippet"
   "emacs-guix"
   "emacs-flycheck"
   "emacs-flycheck-guile"
   "emacs-geiser"
   "emacs-geiser-guile"
   "emacs-multiple-cursors"
   "emacs-magit"
   "emacs-paren-face"
   "emacs-aggressive-indent"
   "emacs-web-mode"
   "emacs-keycast"
   "emacs-gif-screencast"
   "emacs-yaml-mode"
   "emacs-vertico"
   "emacs-marginalia"
   "emacs-embark"
   "emacs-consult"
   "emacs-orderless"
   "emacs-corfu"
   "emacs-ox-reveal"
   "emacs-iedit"
   "emacs-darkroom"
   "emacs-envrc"
   "emacs-htmlize"
   "emacs-eros"
   "emacs-geiser-eros"
   "emacs-geiser-gunit64"
   "emacs-redacted"
   
   ;; Guile
   "guile"
   "guile-readline"
   "guile-colorized"
   "guile-hall"
   ))
